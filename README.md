# Weather analyzer application
# Getting Started
This instruction will give you a copy of the project and help you run it on your local machine and/or web server for development and testing purpose.

## Requirements

Before you start installing the project, you should install below items in your machine:

* [NodeJs](https://nodejs.org/en/)
* @angular/cli >= 10 (```npm install -g @angular/cli```)

### Download and install the project
###### open cmd and run the following commands
1. ``git clone https://arashbahreini@bitbucket.org/arashbahreini/weather_analyzer.git``
2. ``cd .\weather_analyzer\``
3. ``npm install``
4. ``ng serve -o``

## Run test
###### For running unit test execute
Run ``npm run test:local`` which actually runs ``ng test --code-coverage``
  
Note: Tests run on ```Karma``` which has set up in ```Karma.conf.js``` file.

Note: code coverage generate coverage index and in this project **100%** of 
code is covered by unit test.
###### For running e2e test execute
Run ``npm run e2e`` which actually runs ``ng e2e`` command and open browser 
then execute end to end tests of the application

Note: e2e will host on port 4200, so before 
running e2e command you should stop ``ng serve`` command

## Build application
Run ```npm run build``` build the application in un-minified format 
and  ```npm run build:prod``` build application in human unreadable, minified and in --prod version


## PipeLine | Automation
Automation is design in [bitbucket-pipelines.yml](./bitbucket-pipelines.yml) file
and it is a simple pipeline, in order check lint, unit test and finally build 
application in production and AOT way. 
Note: The output of this application is not going to deploy to any server, so last
step of pipeline just generate output in ``dist`` folder.

[![Pipelines](https://img.shields.io/badge/Bitbucket-Pipelines-blue.svg?style=flat)](https://bitbucket.org/arashbahreini/weather_analyzer/addon/pipelines/home) 


### Notes and best practices
* Dashboard page, which gives you brief information about city weathers, is running asynchronously. That means if getting information about a city is faced a problem or takes much time, it does not affect to showing other cities information.
* In the code of applciation I did not use ``any`` type. Except for unit tests.
* Class and interfaces are named according to the best practices
* Every part of the application which is calling an API, is watched by 
error handler, so if any API fail, an appropriate message would be appeared
 for user
* All the components are covered by ``end to end`` test
* The whole application is responsive, feel free to resize your browser
* I used Angular animation to creating more **user friendly** and **eye appealing** 
pages
* Code coverage for unit test is **100%**

## Authors

* **Arash Bahreini**

See also my [GitHub](https://github.com/arashbahreini) profile.
