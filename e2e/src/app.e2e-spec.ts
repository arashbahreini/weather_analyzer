import {AppPage} from './app.po';
import {browser, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getElement('.greeting-text').getText()).toEqual('Welcome to Backbase Weather Analyzer');
  });

  it('Amsterdam should present as the first city in the list', () => {
    page.navigateTo();
    browser.sleep(4000);
    expect(page.getElement('#Amsterdam').getText()).toEqual('Amsterdam');
  });

  it('Amsterdam forecast should present on clicking forecast button', () => {
    page.navigateTo();
    browser.sleep(4000);
    page.getElement('#button_forecast_Amsterdam').click();
    const presentedElement = page.getElement('#forecasts_header_Amsterdam').isPresent();
    expect(presentedElement).toBeTruthy();
  });

  it('When click on one forecast, detail box should be appear at the bottom of the page', () => {
    page.navigateTo();
    browser.sleep(4000);
    page.getElement('#button_forecast_Amsterdam').click();
    page.getElement('#forecasts_content_Amsterdam').click();
    const presentedElement = page.getElement('.forecast-details-section-header').isPresent();
    expect(presentedElement).toBeTruthy();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
