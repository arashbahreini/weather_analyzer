import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Observable, of, throwError} from 'rxjs';
import {GeneralInformation} from './models/weather-map';
import {City} from './models/city';
import {OpenWeatherService} from './services/open-weather.service';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

export class MockOpenWeatherService {
  getCityWeather(id: number): Observable<GeneralInformation> {
    const result = {} as GeneralInformation;
    return of(result);
  }

  getCities(): City[] {
    return [];
  }
}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let service: MockOpenWeatherService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: OpenWeatherService, useClass: MockOpenWeatherService}
      ],
      declarations: [
        AppComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  afterEach(() => {
    component = null;
    fixture = null;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(OpenWeatherService);
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('getCities should be called in onInit', () => {
    spyOn(component, 'getCities');
    component.ngOnInit();
    expect(component.getCities).toHaveBeenCalled();
  });

  it('getCityWeather should be called in onInit', () => {
    spyOn(component, 'getCityWeather');
    component.ngOnInit();
    expect(component.getCityWeather).toHaveBeenCalled();
  });

  it('getCityWeather service should be called when getWeather get call', () => {
    const mockServiceResult = of({
      name: 'testName'
    } as GeneralInformation);

    spyOn(service, 'getCityWeather').and.returnValue(mockServiceResult);
    component.getWeather({id: 1} as City);
    expect(service.getCityWeather).toHaveBeenCalledWith(1);
  });

  it('getCities service should be called when call getCities method', () => {
    spyOn(service, 'getCities').and.returnValue([]);
    component.getCities();
    expect(service.getCities).toHaveBeenCalled();
  });

  it('selectedCity should be change when call showForecasts method', () => {
    const mockTargetValue = {id: 1} as City;
    component.showForecasts(mockTargetValue);
    expect(component.selectedCity).toEqual(mockTargetValue);
  });

  it('isGreetingFinished should set true when call showWeathers', () => {
    component.isGreetingFinished = false;
    component.showWeathers();
    expect(component.isGreetingFinished).toBeTrue();
  });

  it('getCityWeather should call getWeather', () => {
    component.cities = [{} as City];
    spyOn(component, 'getWeather');
    component.getCityWeather();
    expect(component.getWeather).toHaveBeenCalled();
  });

  it('getWeather should act well if getCityWeather fail', () => {
    spyOn(service, 'getCityWeather').and.callFake(() => {
      return throwError(new Error('Fake error'));
    });

    const methodArg = {
      generalInformation: {}
    } as City;
    component.getWeather(methodArg);
    expect(methodArg.generalInformation.hasError).toBeTrue();
  });
});
