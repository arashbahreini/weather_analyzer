import {Component, OnInit} from '@angular/core';
import {OpenWeatherService} from './services/open-weather.service';
import {City} from './models/city';
import {ApiResult} from './models/api-result';
import {GeneralInformation} from './models/weather-map';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fade', [
      state('void', style({opacity: 0})),
      state('*', style({opacity: 1})),
      transition(':enter', animate(`2s ease-out`)),
    ])
  ]
})
export class AppComponent implements OnInit {
  cities: City[] = [];
  isGreetingFinished: boolean;
  selectedCity: City;

  constructor(private openWeatherService: OpenWeatherService) {
  }

  ngOnInit(): void {
    this.getCities();
  }

  getCities(): void {
    this.cities = this.openWeatherService.getCities();
    this.getCityWeather();
  }

  getCityWeather(): void {
    this.cities.forEach(city => {
      this.getWeather(city);
    });
  }

  showWeathers(): void {
    this.isGreetingFinished = true;
  }

  showForecasts(city: City): void {
    this.selectedCity = city;
  }

  getWeather(city: City): void {
    city.generalInformation = new ApiResult<GeneralInformation>();
    city.generalInformation.isLoading = true;
    city.generalInformation.data = {
      name: city.name,
      id: city.id
    } as GeneralInformation;
    this.openWeatherService.getCityWeather(city.id).subscribe(
      (res: GeneralInformation) => {
        city.generalInformation.setData(res);
      }, (error: string) => {
        city.generalInformation.setError(error);
      }
    );
  }
}

