import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CityWeatherComponent} from './city-weather.component';
import {City} from '../models/city';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatTooltipModule} from '@angular/material/tooltip';

describe('CityWeatherComponent', () => {
  let component: CityWeatherComponent;
  let fixture: ComponentFixture<CityWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatTooltipModule],
      declarations: [CityWeatherComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    fixture = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('showForecastsClick should emit showForecasts', () => {
    const mockEmitArg = {} as City;
    component.city = mockEmitArg;
    spyOn(component.showForecasts, 'emit');
    component.showForecastsClick();
    expect(component.showForecasts.emit).toHaveBeenCalledWith(mockEmitArg);
  });

  it('showForecastsClick should set selectedCity value', () => {
    const mockedArg = {id: 1} as City;
    component.city = mockedArg;
    component.showForecastsClick();
    expect(component.selectedCity).toEqual(mockedArg);
  });

  it('clicking on forecast button should call showForecastsClick method', () => {
    component.city = {id: 1} as City;
    spyOn(component, 'showForecastsClick');
    fixture.debugElement.query(By.css('.btn')).nativeElement.click();
    expect(component.showForecastsClick).toHaveBeenCalled();
  });
});
