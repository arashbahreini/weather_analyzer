import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {City} from '../models/city';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent implements OnInit {

  @Input() city: City = {} as City;
  @Input() selectedCity: City;
  @Output() showForecasts: EventEmitter<City> = new EventEmitter<City>();
  @Output() tryAgain: EventEmitter<City> = new EventEmitter<City>();

  constructor() {
  }

  ngOnInit(): void {
  }

  // This method is responsible to send selected city to his parent (appComponent)
  showForecastsClick(): void {
    this.showForecasts.emit(this.city);
    this.selectedCity = this.city;
  }
}
