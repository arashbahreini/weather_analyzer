import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ForecastGroupComponent} from './forecast-group.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {City} from '../models/city';
import {Observable, of, throwError} from 'rxjs';
import {Forecast, List} from '../models/forecast';
import {OpenWeatherService} from '../services/open-weather.service';
import {ApiResult} from '../models/api-result';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

export class MockOpenWeatherService {
  getForeCast(cityId: number): Observable<Forecast> {
    const result = {} as Forecast;
    return of(result);
  }
}

describe('ForecastGroupComponent', () => {
  let component: ForecastGroupComponent;
  let fixture: ComponentFixture<ForecastGroupComponent>;
  let service: MockOpenWeatherService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ForecastGroupComponent],
      providers: [
        {provide: OpenWeatherService, useClass: MockOpenWeatherService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(OpenWeatherService);
  });

  afterEach(() => {
    component = null;
    fixture = null;
  });

  beforeEach(() => {
    component.forecasts = new ApiResult<Forecast>();
    component.forecasts.data = {
      list: [
        {dt_txt: '2020-10-01 09:00:00'} as List,
        {dt_txt: '2020-10-03 00:00:00'} as List,
      ]
    } as Forecast;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnChanges should call getForeCastWeather', () => {
    component.city = {id: 1} as City;
    spyOn(component, 'getForeCastWeather');
    component.ngOnChanges();
    expect(component.getForeCastWeather).toHaveBeenCalled();
  });

  it('getDateByDayFormat should return appropriate string format', () => {
    const methodResult = component.getDateByDayFormat('10/1/2020');
    expect(methodResult).toEqual('Thursday');
  });

  it('getForeCastWeather should call getForeCast service', () => {
    component.city = {id: 1} as City;
    spyOn(service, 'getForeCast').and.returnValue(of({list: []} as Forecast));
    component.getForeCastWeather();
    expect(service.getForeCast).toHaveBeenCalled();
  });

  it('fillGroupDates should push appropriate data into groupDates', () => {
    component.fillGroupDates();
    expect(component.groupDates).toEqual(['10/1/2020', '10/3/2020']);
  });

  it('showForecastDetails should set selectForecast value', () => {
    const methodArg = {dt_txt: 'foo'} as List;
    component.showForecastDetails(methodArg);
    expect(component.selectForecast).toEqual(methodArg);
  });

  it('getDayForecasts should return appropriate data', () => {
    const methodResult = component.getDayForecasts('10/1/2020');
    expect(methodResult).toEqual([{dt_txt: '2020-10-01 09:00:00'} as List]);
  });

  it('getGroupStyle should return appropriate data according to its argument', () => {
    const methodResult = component.getGroupStyle('10/1/2020');
    const acceptableResult = {
      width: '4.9%',
      marginRight: '.3%'
    };
    expect(methodResult).toEqual(acceptableResult);
  });

  it('getItemStyle should return appropriate data according to its argument', () => {
    const methodResult = component.getItemStyle('10/1/2020');
    const acceptableResult = {
      width: '100%',
    };
    expect(methodResult).toEqual(acceptableResult);
  });

  it('getForeCastWeather should act well if getForeCast service fail', () => {
    spyOn(service, 'getForeCast').and.callFake(() => {
      return throwError('fake error');
    });
    component.city = {id: 1} as City;
    component.getForeCastWeather();
    expect(component.componentReadyToShow).toBeTrue();
    expect(component.forecasts.hasError).toBeTrue();
  });
});
