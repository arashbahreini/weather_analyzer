import {Component, Input, OnChanges} from '@angular/core';
import {City} from '../models/city';
import {OpenWeatherService} from '../services/open-weather.service';
import {ApiResult} from '../models/api-result';
import {Forecast, List} from '../models/forecast';
import * as moment from 'moment';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-forecast-group',
  templateUrl: './forecast-group.component.html',
  styleUrls: ['./forecast-group.component.scss'],
  animations: [
    trigger('fade', [
      state('void', style({opacity: 0})),
      state('*', style({opacity: 1})),
      transition(':enter', animate(`500ms ease-out`)),
    ])
  ]
})
export class ForecastGroupComponent implements OnChanges {

  @Input() city: City;

  // Because there are some arithmetic operations in this component, I decide to show the
  // content only when all the operations are performed.
  componentReadyToShow: boolean;
  selectForecast: List;

  forecasts: ApiResult<Forecast> = new ApiResult<Forecast>();
  groupDates: string[] = [];

  constructor(private openWeatherService: OpenWeatherService) {
  }

  // If components input(s) change, load new data from API
  ngOnChanges(): void {
    this.componentReadyToShow = false;
    this.selectForecast = null;
    if (this.city) {
      this.getForeCastWeather();
    }
  }

  // Get a date and return in day format (Monday, etc)
  getDateByDayFormat(date: string): string {
    return moment(new Date(date)).format('dddd');
  }

  // Get forecast information from API according to selected city
  getForeCastWeather(): void {
    this.forecasts.isLoading = true;
    this.openWeatherService.getForeCast(this.city.id).subscribe(
      (res: Forecast) => {
        // The full list has 40 items and implement it in the UI required modal and/or another page.
        // I decided to split the array into fewer elements to being able to present all items in one section
        res.list.splice(20, res.list.length);
        this.forecasts.setData(res);
        this.fillGroupDates();
      }, (error: string) => {
        this.componentReadyToShow = true;
        this.forecasts.setError(error);
      }
    );
  }

  // Separate API-data and group it by day
  fillGroupDates(): void {
    this.forecasts.data.list.forEach((item: List, index: number) => {
      const formattedDate = moment(item.dt_txt).format('l');
      if (!this.groupDates.find(x => x === formattedDate)) {
        this.groupDates.push(formattedDate);
      }
    });
    this.componentReadyToShow = true;
  }

  showForecastDetails(dayForecast: List): void {
    this.selectForecast = dayForecast;
  }

  // To group 20 items by day
  getDayForecasts(date: string): List[] {
    const result: List[] = [];
    this.forecasts.data.list.forEach((item: List) => {
      if (moment(item.dt_txt).format('l') === date) {
        result.push(item);
      }
    });
    return result;
  }

  // This method return dynamic style for forecast item group,
  // then the box resize automatically according to its data
  getGroupStyle(groupDate: string): {} {
    return {
      width: this.forecasts.data.list.filter(x => moment(x.dt_txt).format('l') === groupDate).length * 4.9 + '%',
      marginRight: '.3%'
    };
  }

  // Explained in HTML file
  getItemStyle(groupDate: string): {} {
    return {
      width: 100 / this.getDayForecasts(groupDate).length + '%',
    };
  }
}
