import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GreetingComponent} from './greeting.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('GreetingComponent', () => {
  let component: GreetingComponent;
  let fixture: ComponentFixture<GreetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      declarations: [GreetingComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  afterEach(() => {
    component = null;
    fixture = null;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('showBlinker should be called at onInit', () => {
    spyOn(component, 'showBlinker');
    component.ngOnInit();
    expect(component.showBlinker).toHaveBeenCalled();
  });

  it('presentGreetingMessage should be called after showBlinker', () => {
    jasmine.clock().install(); // first install the clock
    spyOn(component, 'presentGreetingMessage');
    component.showBlinker();
    jasmine.clock().tick(1500); // wait for 1500ms
    expect(component.presentGreetingMessage).toHaveBeenCalled();
    jasmine.clock().uninstall(); // uninstall the clock when test is done
  });

  it('presentGreetingMessage should put 1 character to greetingMessage every X milli second', () => {
    component.greetingMessage = '';
    jasmine.clock().install();
    component.presentGreetingMessage('B');
    jasmine.clock().tick(30);
    expect(component.greetingMessage).toBe('B');
    jasmine.clock().uninstall();
  });

  it('greetingMessage should be empty after 1500ms when showBlinker get called', () => {
    jasmine.clock().install();
    component.showBlinker();
    jasmine.clock().tick(1500);
    expect(component.greetingMessage).toBe('');
    jasmine.clock().uninstall();
  });

  it('animationFinish should emit true if event.toState is true', () => {
    spyOn(component.isFinished, 'emit').withArgs(true);
    component.animationFinish({toState: 'true'});
    expect(component.isFinished.emit).toHaveBeenCalledWith(true);
  });

  it('method should be end if this.greetingMessage.length === message.length', () => {
    component.isGreetingInProgressFinished = 'false';
    component.greetingMessage = 'a';
    jasmine.clock().install();
    component.presentGreetingMessage('a');
    jasmine.clock().tick(40);
    expect(component.isGreetingInProgressFinished).toEqual('true');
    jasmine.clock().uninstall();
  });
});
