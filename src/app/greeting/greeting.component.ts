import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.scss'],
  animations: [
    trigger('greetingAnimation', [
      transition('true <=> false', animate('.5s')),
      state('true', style({
        paddingTop: '1.5vh',
        color: '#878787',
      })),
      state('false', style({
        paddingTop: '20vh',
        color: '#759d01',
      })),
    ]),
  ]
})
export class GreetingComponent implements OnInit {
  @Output() isFinished: EventEmitter<boolean> = new EventEmitter<boolean>();

  greetingMessage: string;
  isGreetingInProgressFinished = 'false';

  ngOnInit(): void {
    this.showBlinker();
  }


  // Show blinker `| | | |` in the main page before typing welcome text
  showBlinker(): void {
    const blinkerInterval = setInterval(() => {
      this.greetingMessage = this.greetingMessage === '|' ? '' : '|';
    }, 200);
    setTimeout(() => {
      clearInterval(blinkerInterval);
      this.greetingMessage = '';
      this.presentGreetingMessage('Welcome to Backbase Weather Analyzer');
    }, 1500);
  }

  // When animation finish talk to his parent and let them know to show other components
  // and continue the process
  animationFinish(event): void {
    if (event.toState === 'true') {
      this.isFinished.emit(true);
    }
  }

  // Type greeting message with setInterval function, then user feels that the text is
  // appearing like somebody is typing it
  presentGreetingMessage(message: string): void {
    const showGreetingInterval = setInterval(() => {
      if (this.greetingMessage.length === message.length) {
        clearInterval(showGreetingInterval);
        this.isGreetingInProgressFinished = 'true';
        return;
      }
      this.greetingMessage += message[this.greetingMessage.length];
    }, 30);
  }

}
