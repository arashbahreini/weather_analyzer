export class ApiResult<T> {
  data: T;
  hasError: boolean;
  isLoading: boolean;
  errorMessage: string;

  constructor() {
    this.hasError = false;
    this.isLoading = true;
  }

  setData(data: T): void {
    this.data = data;
    this.hasError = false;
    this.isLoading = false;
  }

  setError(message: string): void {
    this.hasError = true;
    this.isLoading = false;
    this.errorMessage = message;
  }
}
