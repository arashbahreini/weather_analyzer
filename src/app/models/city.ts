import {GeneralInformation} from './weather-map';
import {ApiResult} from './api-result';

export interface City {
  id: number;
  name: string;
  country: string;
  generalInformation?: ApiResult<GeneralInformation>;
}
