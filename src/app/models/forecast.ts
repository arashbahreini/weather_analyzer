import {Main, Weather, Wind} from './weather-map';

export interface Forecast {
  cod: number;
  message: number;
  cnt: number;
  list: List[];
}

export interface Clouds {
  all: number;
}

export interface List {
  dt: number;
  dt_txt: string;
  main: Main;
  weather: Weather;
  wind: Wind;
  clouds: Clouds;
}



