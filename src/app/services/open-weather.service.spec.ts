import {AppModule} from './../app.module';
import {TestBed} from '@angular/core/testing';
import {HttpClient, HttpClientModule, HttpErrorResponse} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OpenWeatherService} from './open-weather.service';
import {Observable, of} from 'rxjs';
import {GeneralInformation} from '../models/weather-map';
import {Forecast} from '../models/forecast';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {componentFactoryName} from '@angular/compiler';

describe('OpenWeatherService', () => {
  let service: OpenWeatherService;
  const cityId = 2759794;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, HttpClientModule, HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new OpenWeatherService(httpClientSpy as any);
  });

  afterEach(() => {
    service = null;
  });

  it('getCityWeather method should return observable', () => {
    httpClientSpy.get.and.returnValue(of({}));
    service.getCityWeather(cityId).subscribe(res => {
      expect(res).toEqual({} as GeneralInformation);
    });
  });

  it('getForeCast method should return observable', () => {
    httpClientSpy.get.and.returnValue(of({}));
    service.getForeCast(cityId).subscribe(res => {
      expect(res).toEqual({} as Forecast);
    });
  });

  it('getCities should return array with length 5', () => {
    const methodResult = service.getCities();
    expect(methodResult.length).toEqual(5);
  });

  it('handleError should return expected string message', () => {
    const methodResult = service.handleError();
    methodResult.subscribe(res => {
    }, (error) => {
      expect(error).toEqual('Oops! There is something wrong in getting data. Please try again');
    });
  });
});
