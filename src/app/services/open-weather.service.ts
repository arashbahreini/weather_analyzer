import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {GeneralInformation} from '../models/weather-map';
import {Forecast} from '../models/forecast';
import {City} from '../models/city';

@Injectable({providedIn: 'root'})
export class OpenWeatherService {
  private apiWeather = 'http://api.openweathermap.org/data/2.5/weather?id=';
  private apiForecast = 'http://api.openweathermap.org/data/2.5/forecast?id=';
  private ApiToken = '&APPID=ed276a5c9e0c38762eea15fbe0b15885&units=metric';

  constructor(private http: HttpClient) {
  }

  getCityWeather(cityId: number): Observable<GeneralInformation> {
    return this.http.get<GeneralInformation>(this.apiWeather + cityId + this.ApiToken)
      .pipe(catchError(this.handleError));
  }

  getForeCast(cityId: number): Observable<Forecast> {
    return this.http.get<Forecast>(this.apiForecast + cityId + this.ApiToken)
      .pipe(catchError(this.handleError));
  }

  // General error handler, if any API fail, instead of showing un-readable backend
  // message, this method return a nice and readable message.
  handleError(): Observable<never> {
    return throwError(
      'Oops! There is something wrong in getting data. Please try again');
  }

  // City list is hard coded
  getCities(): City[] {
    const cities: City[] = [];
    cities.push({
      id: 2759794,
      name: 'Amsterdam',
      country: 'NL',
    });

    cities.push({
      id: 2968815,
      name: 'Paris',
      country: 'FR',
    });

    cities.push({
      id: 6542283,
      name: 'Milan',
      country: 'IT',
    });

    cities.push({
      id: 2950159,
      name: 'Berlin',
      country: 'DE',
    });

    cities.push({
      id: 2618425,
      name: 'Copenhagen',
      country: 'DK',
    });

    return cities;
  }
}
